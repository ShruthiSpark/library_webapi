﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper; 

namespace Library.Repository
{
    public class LibraryRepository
    {
        private readonly IMapper _mapper;

        public LibraryRepository(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IEnumerable<Book> BookList
        {
            get
            {

                List<Book> bookresponse = new List<Book>();
                bookresponse.Add(new Book()
                {
                    BookName = "C#",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$150"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "English",
                    Author = "Wren and Martin",
                    Available = "Yes",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "JAVA",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Mulesoft",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$150"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Realm",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$350"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Jquey",
                    Author = "Pearson",
                    Available = "No",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Angular",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Sql",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "MVC",
                    Author = "Pearson",
                    Available = "No",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "JME",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$250"
                });

                return bookresponse;
            }

            
        }
    }
    public class Book
    {
        public string BookName { get; set; }
        public string Author { get; set; }
        public string Available { get; set; }
        public string Price { get; set; }

    }
}
