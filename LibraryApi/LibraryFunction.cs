using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.ApplicationInsights;
using System.Collections.Generic;
using System.Net;

namespace LibraryApi
{
    public class LibraryBookGet
    {
        //Commented because didn't have enough space to install app insights Nugget package
        public LibraryBookGet(TelemetryConfiguration telemetryConfiguratio)
        {

            _telemetryClient = new TelemetryClient(telemetryConfiguration);
        }
        [FunctionName("GetLibrary")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", null, Route = "getlibrarybooks")] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C#  HTTP getlibrarybooks trigger function processed a request.");

            string name = req.Query["name"];
            try
            {
                IEnumerable<Book> books = await Task.Run(() => BookList);
                return new JsonResult(books);
            }
            catch (Exception ex)
            {
                //throw ex.Message;
                // _telemetryClient.TrackException(ex);
                return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                
            }
            
        }

        public IEnumerable<Book> BookList
        {
            get
            {

                List<Book> bookresponse = new List<Book>();
                bookresponse.Add(new Book()
                {
                    BookName = "C#",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$150"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "English",
                    Author = "Wren and Martin",
                    Available = "Yes",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "JAVA",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Mulesoft",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$150"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Realm",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$350"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Jquey",
                    Author = "Pearson",
                    Available = "No",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Angular",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "Sql",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "MVC",
                    Author = "Pearson",
                    Available = "No",
                    Price = "$250"
                });
                bookresponse.Add(new Book()
                {
                    BookName = "JME",
                    Author = "Pearson",
                    Available = "Yes",
                    Price = "$250"
                });

                return bookresponse;
            }


        }
    }
    public class Book
    {
        public string BookName { get; set; }
        public string Author { get; set; }
        public string Available { get; set; }
        public string Price { get; set; }

    }
}

