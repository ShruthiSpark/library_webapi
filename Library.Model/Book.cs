
using System;

namespace Book.Models
{
    public class Book
    {
        public string BookName { get; set; }
        public string Author { get; set; }
        public string Available { get; set; }
        public string Price { get; set; }

    }
}
